Name:           perl-File-ShareDir-Install
Version:        0.14
Release:        2
Summary:        Install shared files

License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/File-ShareDir-Install
Source0:        https://cpan.metacpan.org/authors/id/E/ET/ETHER/File-ShareDir-Install-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  make perl-generators perl-interpreter perl(ExtUtils::MakeMaker) 
BuildRequires:  perl(Carp) perl(Exporter) perl(File::Spec) perl(IO::Dir) perl(strict) perl(warnings)
BuildRequires:  perl(Test::More) perl(File::Path)


%{?perl_default_filter}

%description
File::ShareDir::Install allows you to install read-only data files from a
distribution. It is a companion module to File::ShareDir, which allows you
to locate these files after installation.


%package_help


%prep
%autosetup -n File-ShareDir-Install-%{version} -p1


%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
%make_build


%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*


%check
make test


%files
%license LICENSE
%{perl_vendorlib}/*


%files help
%doc Changes CONTRIBUTING README
%{_mandir}/man3/*.3*


%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.14-2
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Jul 20 2023 leeffo <liweiganga@uniuontech.com> - 0.14-1
- upgrade to version 0.14

* Thu Nov 28 2019 Qianbiao.NG <Qianbiao.NG@turnbig.net> - 0.13-4
- Repackage for openEuler OS
